import { Component, OnInit } from '@angular/core';
import { Tutorial } from 'src/app/models/tutorial.model';
import { TutorialService } from 'src/app/services/tutorial.service';

@Component({
  selector: 'app-tutorials-list',
  templateUrl: './tutorials-list.component.html',
  styleUrls: ['./tutorials-list.component.css']
})
export class TutorialsListComponent implements OnInit {

  tutorials?: Tutorial[];
  currentTutorial: Tutorial = {};
  currentIndex = -1;
  title = '';

  constructor(private tS: TutorialService) { }

  ngOnInit(): void {
    let t = this;
    t.retrieveTutorial();
  }

  retrieveTutorial(): void {
    let t = this;
    t.tS.getAll().subscribe({
      next: (r_) => {
        t.tutorials = r_;
        console.log("success", r_);
      },
      error: (e) => {
        console.error("err", e);
      }
    });
  }

  refresh(): void {
    let t = this;
    t.retrieveTutorial();
    t.currentTutorial = {};
    t.currentIndex = -1;
  }

  setActTutorial(tutorial: Tutorial, ix: number): void {
    let t = this;
    t.currentTutorial = tutorial;
    t.currentIndex = ix;
  }

  remAllTutorials(): void {
    let t = this;
    t.tS.deleteAll().subscribe({
      next: (r) => {
        t.refresh();
        console.log('success', r);

      },
      error: (e) => {
        console.error('error', e);

      }
    });
  }

  srchTitle(): void {
    let t = this;
    t.currentTutorial = {};
    t.currentIndex = -1;
    t.tS.findByTitle(t.title).subscribe({
      next: (r) => {
        t.tutorials = r;
        console.log("success", r);
      },
      error: (e) => {
        console.error("error", e);
      }
    });
  }

}
