import { Component, OnInit } from '@angular/core';
import { Tutorial } from 'src/app/models/tutorial.model';
import { TutorialService } from 'src/app/services/tutorial.service';

@Component({
  selector: 'app-add-tutorial',
  templateUrl: './add-tutorial.component.html',
  styleUrls: ['./add-tutorial.component.css']
})
export class AddTutorialComponent implements OnInit {

  tut: Tutorial = {
    title: '',
    description: '',
    published: false
  };
  submitted = false;

  constructor(private ts: TutorialService) { }

  ngOnInit(): void {
  }

  svTutorial(): void {
    let t = this;
    const d_ = {
      title: t.tut.title,
      desciption: t.tut.description
    };
    t.ts.create(d_)
      .subscribe({
        next: (r) => {
          console.log("success", r);
          t.submitted = true;
        },
        error: (e) => {
          console.error("error", e);
        }
      });
  }

  newTutorial(): void {
    let t = this;
    t.submitted = false;
    t.tut = {
      title: '',
      description: '',
      published: false
    };
  }
}
