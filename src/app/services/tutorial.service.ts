import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tutorial } from '../models/tutorial.model';

const baseUrl = 'http://localhost:8080/tutorials';

@Injectable({
  providedIn: 'root'
})

export class TutorialService {

  constructor(private h: HttpClient) { }

  getAll(): Observable<Tutorial[]> {
    let $g = this.h;
    return $g.get<Tutorial[]>(baseUrl);
  }

  get(id: any): Observable<Tutorial> {
    let $g = this.h;
    return $g.get<Tutorial>(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    let $g = this.h;
    return $g.post(baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    let $g = this.h;
    return $g.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    let $g = this.h;
    return $g.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    let $g = this.h;
    return $g.delete(baseUrl);
  }

  findByTitle(title: any): Observable<Tutorial[]> {
    let $g = this.h;
    return $g.get<Tutorial[]>(`${baseUrl}?title=${title}`);
  }
}
