# My First Angular

## Getting started
clone project.
npm install.
read usage for run.

## Name
Rinto Prabowo (bowo.jach@gmail.com)

## Description
Script ini dibuat untuk pembelajaran atau contoh menggunakan angular cli versi 14

## Usage
`npm run s` untuk menjalankan aplikasi \
`npx json-server --watch jsonServer/db.json --port 8080` untuk menjalankan simulasi server json

## Support
Aplikasi ini dibuat mengikuti tutorial https://www.bezkoder.com/angular-14-crud-example/

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
myself\
bezKoder (https://www.bezkoder.com/)

## Authors and acknowledgment
Thanks BezKoder, your tutorials very helpfull myself

## License
For open source projects, say how it is licensed.

## Project status
Closed
